﻿Imports System.ComponentModel
Imports System.IO
Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.Threading
Imports Squirrel

Public Class MainForm
    'Class references
    Private ConfigurationClass As Configuration
    Private AuthenticationClass As Authentication
    Private HelperClass As Helper
    Private CaptureClass As Capture
    Private UploadClass As Upload

    'Diagnostic Related
    Private LocalProcess As Process = Process.GetCurrentProcess
    Private LocalPerformanceCounter As PerformanceCounter = New PerformanceCounter
    Private LastPolledCPUPercent As Double
    Private DefaultProcessorAffinity As Integer
    Private LastProcessAdjustment As Integer = 0

    'App space
    Private MainLoopIsComplete As Boolean = False
    Private KeybindThread As Thread
    Private DetectionDelayRate As Integer = 30

    'Form window constants
    Const WM_SYSCOMMAND As Int32 = &H112
    Const SC_MINIMIZE As Int32 = &HF020
    Const SC_RESTORE As Int32 = &HF120

    'User sapce
    Public WindowIsActive As Boolean = False
    Public UserWantsToClose As Boolean = False


    <STAThread()>
    Private Sub Constructor(sender As Object, e As EventArgs) Handles MyBase.Load
        'Set threading apartment state so that threaded events can access system calls
        Threading.Thread.CurrentThread.SetApartmentState(Threading.ApartmentState.STA)

        'Disable Safe Cross Thread Check
        CheckForIllegalCrossThreadCalls = False

        'Debug mesasge
        EasyLog("Application\Constructor", "Application has been started")

        'Change Form Appearance
        Me.Icon = My.Resources.favicon

        'Change Notification Icon
        MainNotificationIcon.Icon = My.Resources.favicon

        'Initialize Classes
        ConfigurationClass = New Configuration
        AuthenticationClass = New Authentication(ConfigurationClass)
        HelperClass = New Helper(Me, ConfigurationClass)
        CaptureClass = New Capture(Me, ConfigurationClass)
        UploadClass = New Upload(Me, HelperClass, ConfigurationClass)

        'Check for an update.
        UpdaterAsync()

        'Check to make sure the user is logged in
        If My.Settings.AccountEmailAddress = "" Then ChangeCredentialsToolStripMenuItem_Click(Nothing, Nothing)

        'Load the settings
        LoadSettings()

        'Populate the comboboxes
        HelperClass.PopulateCombobox(UploadFileDialogComboBox)
        HelperClass.PopulateCombobox(UploadCurrentWindowComboBox)
        HelperClass.PopulateCombobox(UploadFullscreenComboBox)
        HelperClass.PopulateCombobox(UploadScreenSelectionComboBox)
        HelperClass.PopulateCombobox(UploadClipboardComboBox)

        'Load Comboboxes values
        HelperClass.LoadComboboxSetting(UploadFileDialogComboBox, My.Settings.UploadFileDialogKeybind)
        HelperClass.LoadComboboxSetting(UploadCurrentWindowComboBox, My.Settings.CaptureCurrentWindowKeybind)
        HelperClass.LoadComboboxSetting(UploadFullscreenComboBox, My.Settings.CaptureFullscreenKeybind)
        HelperClass.LoadComboboxSetting(UploadScreenSelectionComboBox, My.Settings.CaptureSelectionKeybind)
        HelperClass.LoadComboboxSetting(UploadClipboardComboBox, My.Settings.UploadClipboardKeybind)

        'Load Modifiers
        CheckBox1.Checked = My.Settings.ControlKeyRequired
        CheckBox2.Checked = My.Settings.ShiftKeyRequired

        Select Case My.Settings.PrintScreenKeyAction
            Case 0
                RadioButton1.Checked = True
            Case 1
                RadioButton2.Checked = True
            Case 2
                RadioButton3.Checked = True
        End Select

        'Get the default processor affinity.
        DefaultProcessorAffinity = LocalProcess.ProcessorAffinity

        'Create Keybind Thread
        KeybindThread = New Thread(AddressOf KeybindWorker_DoWork)
        KeybindThread.SetApartmentState(ApartmentState.STA)
        KeybindThread.IsBackground = True

        'Start Background Workers
        ApplicationStatisticWorker.RunWorkerAsync()
        UpdateIntervalWorker.RunWorkerAsync()
        KeybindThread.Start()

        'Define that we can actually start changing values and giving a damn
        MainLoopIsComplete = True

        HideFormWorker.RunWorkerAsync()

    End Sub

    Private Sub LoadSettings()
        'Encryption Checkbox
        EncryptUploadsToolStripMenuItem.Checked = My.Settings.UploadEncryption

        'File Format
        FileFormatComboBox.SelectedItem = My.Settings.ImageFormat

        'Process Priority
        DynamicallyAdjustProcessPriorityToolStripMenuItem.Checked = My.Settings.DynamicProcessPriority

    End Sub

    Public Shared Sub EasyLog(ByVal what As String, ByVal message As String)
        Debugger.Log(1, 1, "[" + Date.Now + "]-[" + what + "]: " + message + vbNewLine)
    End Sub

    Private Sub ApplicationStatisticWorker_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles ApplicationStatisticWorker.DoWork

        'Configure some attributes
        With LocalPerformanceCounter
            .CategoryName = "Processor"
            .CounterName = "% Processor Time"
            .InstanceName = "_Total"
        End With

        While True
            'Get the Processes memory usage.
            MemoryUsageToolStripMenuItem.Text = "Memory Usage: " + Math.Round(LocalProcess.WorkingSet64 / 1024 / 1024, 2).ToString + " MB"

            'Get the current CPU usage
            LastPolledCPUPercent = LocalPerformanceCounter.NextValue
            CPUUsageToolStripMenuItem.Text = "CPU Usage: " + Math.Round(LastPolledCPUPercent, 2).ToString + "%"

            'Determine if we should dynamically change the process priority
            If My.Settings.DynamicProcessPriority Then
                'Dynamic process prioritization
                If LastPolledCPUPercent > 90.0 Then
                    If LocalProcess.ProcessorAffinity <> 1 Then
                        EasyLog("Application\Diagnostic", "System CPU has exceeded threshold - lowering affinity")
                        LocalProcess.ProcessorAffinity = 1
                        LastProcessAdjustment = 0
                    ElseIf LocalProcess.PriorityClass <> ProcessPriorityClass.BelowNormal Then
                        EasyLog("Application\Diagnostic", "System CPU has exceeded threshold - lowering priority")
                        LocalProcess.PriorityClass = ProcessPriorityClass.BelowNormal
                        LastProcessAdjustment = 0
                    End If
                Else
                    If LocalProcess.ProcessorAffinity <> DefaultProcessorAffinity Then
                        EasyLog("Application\Diagnostic", "System CPU has returned to a normal state - normalizing affinity (was: " + LocalProcess.ProcessorAffinity.ToString + ", new:" + DefaultProcessorAffinity.ToString + ")")
                        LocalProcess.ProcessorAffinity = DefaultProcessorAffinity
                        LastProcessAdjustment = 0
                    ElseIf LocalProcess.PriorityClass <> ProcessPriorityClass.Normal Then
                        EasyLog("Application\Diagnostic", "System CPU has returned to a normal state - normalizing priority")
                        LocalProcess.PriorityClass = ProcessPriorityClass.Normal
                        LastProcessAdjustment = 0
                    End If
                End If

                'Update the affinity label
                ProcessAffinityToolStripMenuItem.Text = "Process Affinity: " + LocalProcess.ProcessorAffinity.ToString

                'Update the adjustment counter label
                LastProcessAdjustmentToolStripMenuItem.Text = "Process priority last adjusted " + LastProcessAdjustment.ToString + " second(s) ago"
            Else
                LastProcessAdjustmentToolStripMenuItem.Text = "Not optimizing process priority"
            End If

            'Increase the diagnostic counter
            LastProcessAdjustment += 1

            'Wait exactly one second before continuing
            Threading.Thread.Sleep(1000)
        End While
    End Sub

    <DllImport("user32.dll")> Shared Function GetAsyncKeyState(ByVal vKey As System.Windows.Forms.Keys) As Short
    End Function

    Private Sub KeybindWorker_DoWork()
        While True
            While WindowIsActive = False
                'Here we should detect for system level calls

                Dim Result As Integer
                For KeyCode = 1 To 255
                    Result = GetAsyncKeyState(KeyCode)
                    If Result = -32767 Then
                        'Handle special cases first.

                        'Print Screen key
                        If GetAsyncKeyState(44) Then
                            Select Case My.Settings.PrintScreenKeyAction
                                Case 1
                                    UploadClass.Upload(CaptureClass.Fullscreen)
                                    GoTo cond_end
                                Case 2
                                    'TODO: Implement
                                    CaptureClass.Selection()
                                    GoTo cond_end

                            End Select
                        End If

                        'Check for requirements
                        If My.Settings.ControlKeyRequired = False And My.Settings.ShiftKeyRequired = False Then ' CTRL AND SHIFT DISABLED

                            'File Dialog
                            If GetAsyncKeyState(My.Settings.UploadFileDialogKeybind) Then
                                UploadClass.Upload(CaptureClass.File)
                                GoTo cond_end
                            End If

                            'Current Window
                            If GetAsyncKeyState(My.Settings.CaptureCurrentWindowKeybind) Then
                                UploadClass.Upload(CaptureClass.CurrentWindow)
                                GoTo cond_end
                            End If

                            'Fullscreen
                            If GetAsyncKeyState(My.Settings.CaptureFullscreenKeybind) Then
                                UploadClass.Upload(CaptureClass.Fullscreen)
                                GoTo cond_end
                            End If

                            'Selection
                            If GetAsyncKeyState(My.Settings.CaptureSelectionKeybind) Then

                                GoTo cond_end
                            End If

                            'Clipboard
                            If GetAsyncKeyState(My.Settings.UploadClipboardKeybind) Then
                                UploadClass.Upload(CaptureClass.Clipboard)
                                GoTo cond_end
                            End If

                        ElseIf My.Settings.ControlKeyRequired And My.Settings.ShiftKeyRequired = False Then 'CTRL required

                            If My.Computer.Keyboard.CtrlKeyDown And My.Computer.Keyboard.ShiftKeyDown = False Then
                                'File Dialog
                                If GetAsyncKeyState(My.Settings.UploadFileDialogKeybind) Then
                                    UploadClass.Upload(CaptureClass.File)
                                    GoTo cond_end
                                End If

                                'Current Window
                                If GetAsyncKeyState(My.Settings.CaptureCurrentWindowKeybind) Then
                                    UploadClass.Upload(CaptureClass.CurrentWindow)
                                    GoTo cond_end
                                End If

                                'Fullscreen
                                If GetAsyncKeyState(My.Settings.CaptureFullscreenKeybind) Then
                                    UploadClass.Upload(CaptureClass.Fullscreen)
                                    GoTo cond_end
                                End If

                                'Selection
                                If GetAsyncKeyState(My.Settings.CaptureSelectionKeybind) Then

                                    GoTo cond_end
                                End If

                                'Clipboard
                                If GetAsyncKeyState(My.Settings.UploadClipboardKeybind) Then
                                    UploadClass.Upload(CaptureClass.Clipboard)
                                    GoTo cond_end
                                End If
                            End If
                        End If

                    ElseIf My.Settings.ControlKeyRequired = False And My.Settings.ShiftKeyRequired = True Then 'Shift Required

                        If My.Computer.Keyboard.CtrlKeyDown = False And My.Computer.Keyboard.ShiftKeyDown Then
                            'File Dialog
                            If GetAsyncKeyState(My.Settings.UploadFileDialogKeybind) Then
                                UploadClass.Upload(CaptureClass.File)
                                GoTo cond_end
                            End If

                            'Current Window
                            If GetAsyncKeyState(My.Settings.CaptureCurrentWindowKeybind) Then
                                UploadClass.Upload(CaptureClass.CurrentWindow)
                                GoTo cond_end
                            End If

                            'Fullscreen
                            If GetAsyncKeyState(My.Settings.CaptureFullscreenKeybind) Then
                                UploadClass.Upload(CaptureClass.Fullscreen)
                                GoTo cond_end
                            End If

                            'Selection
                            If GetAsyncKeyState(My.Settings.CaptureSelectionKeybind) Then

                                GoTo cond_end
                            End If

                            'Clipboard
                            If GetAsyncKeyState(My.Settings.UploadClipboardKeybind) Then
                                UploadClass.Upload(CaptureClass.Clipboard)
                                GoTo cond_end
                            End If
                        End If

                    ElseIf My.Settings.ControlKeyRequired And My.Settings.ShiftKeyRequired Then

                        If My.Computer.Keyboard.CtrlKeyDown And My.Computer.Keyboard.ShiftKeyDown Then
                            'File Dialog
                            If GetAsyncKeyState(My.Settings.UploadFileDialogKeybind) Then
                                UploadClass.Upload(CaptureClass.File)
                                GoTo cond_end
                            End If

                            'Current Window
                            If GetAsyncKeyState(My.Settings.CaptureCurrentWindowKeybind) Then
                                UploadClass.Upload(CaptureClass.CurrentWindow)
                                GoTo cond_end
                            End If

                            'Fullscreen
                            If GetAsyncKeyState(My.Settings.CaptureFullscreenKeybind) Then
                                UploadClass.Upload(CaptureClass.Fullscreen)
                                GoTo cond_end
                            End If

                            'Selection
                            If GetAsyncKeyState(My.Settings.CaptureSelectionKeybind) Then

                                GoTo cond_end
                            End If

                            'Clipboard
                            If GetAsyncKeyState(My.Settings.UploadClipboardKeybind) Then
                                UploadClass.Upload(CaptureClass.Clipboard)
                                GoTo cond_end
                            End If
                        End If

                    End If
                Next
cond_end:

                'Sleep an normal interval
                Threading.Thread.Sleep(DetectionDelayRate)
            End While
            'We can sleep a decent amount, since the window should not work while open
            Threading.Thread.Sleep(50)
        End While
    End Sub

    Private Sub EncryptUploadsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles EncryptUploadsToolStripMenuItem.Click
        If MainLoopIsComplete Then
            My.Settings.UploadEncryption = EncryptUploadsToolStripMenuItem.Checked
            My.Settings.Save()

            If EncryptUploadsToolStripMenuItem.Checked Then
                MainNotificationIcon.BalloonTipIcon = ToolTipIcon.Info
                MainNotificationIcon.BalloonTipTitle = "Symmetric Encryption Enabled"
                MainNotificationIcon.BalloonTipText = "All future uploads to the server will now be encrypted"
                MainNotificationIcon.ShowBalloonTip(ConfigurationClass.DefaultNotificationTimeout)

                'Log
                EasyLog("Application\EncryptionToggle", "Encryption has been enabled")
            Else
                EasyLog("Application\EncryptionToggle", "Encryption has been disabled")
            End If
        End If
    End Sub

    Private Sub DynamicallyAdjustProcessPriorityToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DynamicallyAdjustProcessPriorityToolStripMenuItem.Click
        If MainLoopIsComplete Then
            My.Settings.DynamicProcessPriority = DynamicallyAdjustProcessPriorityToolStripMenuItem.Checked
            My.Settings.Save()
        End If
    End Sub

    Private Sub FileFormatComboBox_Click(sender As Object, e As EventArgs) Handles FileFormatComboBox.SelectedIndexChanged
        If MainLoopIsComplete Then
            My.Settings.ImageFormat = FileFormatComboBox.SelectedItem
            My.Settings.Save()
        End If
    End Sub

    Private Sub ExitToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExitToolStripMenuItem.Click
        Application.Exit()
    End Sub

    Private Sub RestartApplicationToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RestartApplicationToolStripMenuItem.Click
        Application.Restart()
    End Sub

    Private Sub MainForm_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        e.Cancel = True
        WindowIsActive = False
        Me.Hide()
    End Sub

    Private Sub ExitToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles ExitToolStripMenuItem1.Click
        UserWantsToClose = True
        My.Settings.Save()
        Process.GetCurrentProcess.Close()
    End Sub

    Private Sub MainNotificationIcon_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles MainNotificationIcon.MouseDoubleClick
        Me.Show()
    End Sub

    Protected Overrides Sub WndProc(ByRef m As Message)
        If m.Msg = WM_SYSCOMMAND Then
            If m.WParam.ToInt32 = SC_MINIMIZE Then
                'User clicked "minimize"
                Me.Hide()
            End If
        End If
        MyBase.WndProc(m)
    End Sub

    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox1.CheckedChanged
        If MainLoopIsComplete Then
            My.Settings.ControlKeyRequired = CheckBox1.Checked
            My.Settings.Save()
        End If
    End Sub

    Private Sub CheckBox2_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox2.CheckedChanged
        If MainLoopIsComplete Then
            My.Settings.ControlKeyRequired = CheckBox1.Checked
            My.Settings.Save()
        End If
    End Sub

    Private Sub ChangeCredentialsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ChangeCredentialsToolStripMenuItem.Click
redo_authentication:
        My.Settings.AccountEmailAddress = InputBox("Please enter your email address", "Pitter", My.Settings.AccountEmailAddress)
        My.Settings.AccountPassword = InputBox("Please enter your password", "Pitter", My.Settings.AccountPassword)
        My.Settings.Save()

        If Not AuthenticationClass.Authenticate Then
            MainNotificationIcon.BalloonTipIcon = ToolTipIcon.Error
            MainNotificationIcon.BalloonTipTitle = "Invalid Username or Password"
            MainNotificationIcon.BalloonTipText = "The credentials you have provided are invalid. Please try again."
            MainNotificationIcon.ShowBalloonTip(5000)
            GoTo redo_authentication
        End If

        MainNotificationIcon.BalloonTipIcon = ToolTipIcon.Info
        MainNotificationIcon.BalloonTipTitle = "Login Successful"
        MainNotificationIcon.BalloonTipText = "Pitter has successfully authenticated with the server."
        MainNotificationIcon.ShowBalloonTip(5000)
    End Sub

    Private Sub RadioButton1_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton1.CheckedChanged
        If MainLoopIsComplete Then
            My.Settings.PrintScreenKeyAction = 0
            My.Settings.Save()
        End If
    End Sub

    Private Sub RadioButton2_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton2.CheckedChanged
        If MainLoopIsComplete Then
            My.Settings.PrintScreenKeyAction = 1
            My.Settings.Save()
        End If
    End Sub

    Private Sub RadioButton3_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton3.CheckedChanged
        If MainLoopIsComplete Then
            My.Settings.PrintScreenKeyAction = 2
            My.Settings.Save()
        End If
    End Sub

    Private Sub UpdateIntervalWorker_DoWork(sender As Object, e As DoWorkEventArgs) Handles UpdateIntervalWorker.DoWork
        'Placeholder boolean for checking if a game is found
        Dim GameIsFound As Boolean

        'The default delay, grabbed from the application start.
        Dim DefaultDelay = DetectionDelayRate

        'The delay of when a game is found
        Dim PerformanceDeleay = 10

        'Start detecting
        While True
            'Reset the found boolnea
            GameIsFound = False
            For Each ProcessName In HelperClass.HighPerofrmanceProcesses
                If Process.GetProcessesByName(ProcessName).Count <> 0 Then
                    'A game has been found - signal it
                    GameIsFound = True

                    'break out of the loop so we don't chew CPU
                    GoTo break_loop
                End If
            Next

            'Reference point of which we jump to
break_loop:

            'Check to see if a game has been found.
            If GameIsFound Then
                If DetectionDelayRate <> PerformanceDeleay Then
                    'Debug message
                    MainForm.EasyLog("Application\High Performance Detector", "A game is running - setting delay to " + PerformanceDeleay.ToString)

                    'Change the interval to PerformanceDeleay milliseconds
                    DetectionDelayRate = PerformanceDeleay

                    'Tell the user that they are now in the fast mode
                    MainNotificationIcon.BalloonTipIcon = ToolTipIcon.Info
                    MainNotificationIcon.BalloonTipTitle = "High performance mode enabled"
                    MainNotificationIcon.BalloonTipText = "Pitter has detected a game and will listen for screen capture events faster than normal"
                    MainNotificationIcon.ShowBalloonTip(5000)
                End If
            Else
                'Check if there is no longer a game running - if not then we should revert back to normal cpu usage.
                If DetectionDelayRate <> DefaultDelay Then
                    'Debug message
                    MainForm.EasyLog("Application\High Performance Detector", "The game has exited - setting delay back to " + DefaultDelay.ToString)

                    'Restore the default rate
                    DetectionDelayRate = DefaultDelay

                    'Let the user know
                    MainNotificationIcon.BalloonTipIcon = ToolTipIcon.Info
                    MainNotificationIcon.BalloonTipTitle = "Returned to normal mode"
                    MainNotificationIcon.BalloonTipText = "Pitter is no longer working overtime to listen for screen capture events. Hope your game was fun!"
                    MainNotificationIcon.ShowBalloonTip(5000)
                End If
            End If

            'Sleep
            Thread.Sleep(15000)
        End While
    End Sub

    Public Async Sub UpdaterAsync()

        If File.Exists(Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly.Location), "..", "Update.exe")) Then
            Using Manager As New UpdateManager(ConfigurationClass.SquirrelUpdateURL)
                Await Manager.UpdateApp()
            End Using
        End If

    End Sub

    Private Sub HideFormWorker_DoWork(sender As Object, e As DoWorkEventArgs) Handles HideFormWorker.DoWork
        Thread.Sleep(20)
        Me.Hide()
    End Sub
End Class
