﻿Imports System.IO
Imports System.IO.Compression
Imports System.Runtime.InteropServices

Public Class Capture
    Private ParentClassReference As MainForm
    Private ConfigurationClassReference As Configuration
    Private WithEvents MouseDetector As New MouseDetector

    Private ScaledXAxis, ScaledYAxis As Decimal
    Dim NeutalClickingPoint As New Point(-1, -1)
    Dim ClickedPosition1, ClickedPosition2 As Point
    Dim Waiter1, waiter2

    Public Structure ImageSelectionRectangle
        Public left As Integer
        Public top As Integer
        Public right As Integer
        Public bottom As Integer
    End Structure

    Declare Function GetDesktopWindow Lib "user32.dll" () As IntPtr
    Declare Function GetWindowRect Lib "user32.dll" (ByVal hwnd As IntPtr, ByRef lpRect As ImageSelectionRectangle) As Int32
    Declare Function GetForegroundWindow Lib "user32.dll" Alias "GetForegroundWindow" () As IntPtr

    <DllImport("user32.dll", EntryPoint:="GetActiveWindow", SetLastError:=True, CharSet:=CharSet.Unicode, ExactSpelling:=True, CallingConvention:=CallingConvention.StdCall)>
    Public Shared Function GetActiveWindowHandle() As System.IntPtr
    End Function

    Public Sub New(ByVal PassedMainClass As MainForm, ByVal LocalConfigurationClassReference As Configuration)
        ParentClassReference = PassedMainClass
        ConfigurationClassReference = LocalConfigurationClassReference

        'Create DPI Measurements for the whole class.
        Dim ScalingPlaceholder As Graphics = ParentClassReference.CreateGraphics
        ScaledXAxis = ScalingPlaceholder.DpiX / 96.0F
        ScaledYAxis = ScalingPlaceholder.DpiY / 96.0F
    End Sub

    Public Function File()
        Using LocalOpenFileDialog As New OpenFileDialog
            LocalOpenFileDialog.ShowDialog()
            Return LocalOpenFileDialog.FileName
        End Using
    End Function

    Public Function CurrentWindow()
        'Initialize a new structure, as defined above, this will contain the base of the region of which we should capture
        Dim LocalWindowRectangle As New ImageSelectionRectangle

        'Target the window
        GetWindowRect(GetForegroundWindow, LocalWindowRectangle)

        'Placeholder values for the bitmap
        Dim TargetedWindowWidth As Integer = LocalWindowRectangle.right - LocalWindowRectangle.left
        Dim TargetedWindowHeight As Integer = LocalWindowRectangle.bottom - LocalWindowRectangle.top

        'Create the bitmap that will hold the image.
        Dim PlaceholderBitmap As New Bitmap(TargetedWindowWidth, TargetedWindowHeight)
        Dim LocalGraphics As Graphics = Graphics.FromImage(PlaceholderBitmap)

        'Copy the screen's contents into the graphics variable.
        LocalGraphics.CopyFromScreen(LocalWindowRectangle.left, LocalWindowRectangle.top, 0, 0, New Size(TargetedWindowWidth, TargetedWindowHeight))

        'Save the graphics back to the bitmap.
        LocalGraphics.Save()

        'Generaete a filename
        Dim LocalDate As DateTime = DateTime.Now
        Dim Filename As String = String.Format("WindowCapture_{0}-{1}-{2}_{3}{4}{5}.{6}", LocalDate.Year, LocalDate.Month, LocalDate.Day, LocalDate.Hour, LocalDate.Minute, LocalDate.Second, My.Settings.ImageFormat.ToLower)
        Dim IntendedSaveLocation As String = Path.Combine(ConfigurationClassReference.UploadsDirectory, Filename)

        'Save the bitmap.
        PlaceholderBitmap.Save(IntendedSaveLocation, ImageFormatToImageType(My.Settings.ImageFormat))

        'Return to the caller the file location
        Return IntendedSaveLocation
    End Function

    Public Function Fullscreen()
        'Get the total available size
        Dim PixelsInWidth = Screen.AllScreens.Sum(Function(CurrentScreen As Screen) CurrentScreen.Bounds.Width)
        Dim PixelsInHeight = Screen.AllScreens.Max(Function(CurrentScreen As Screen) CurrentScreen.Bounds.Height)

        'Create placeholder variables
        Dim PlaceholderBitmap As New Bitmap(PixelsInWidth, PixelsInHeight)
        Dim LocalGraphics As Graphics

        'Set a reference to the bitmap inside of the graphics variable
        LocalGraphics = Graphics.FromImage(PlaceholderBitmap)

        'Perform mathematical calculations of the region we need to select
        Dim ScaledXRegion = Math.Round(PixelsInWidth * ScaledXAxis)
        Dim ScaledYRegion = Math.Round(PixelsInHeight * ScaledYAxis)

        Dim ScaledScreenSize = New Size(ScaledXRegion, ScaledYRegion)

        'Copy the data from the screen
        LocalGraphics.CopyFromScreen(0, 0, 0, 0, ScaledScreenSize, CopyPixelOperation.SourceCopy)

        'Save the graphics to the bitmap
        LocalGraphics.Save()

        LocalGraphics.Dispose()

        'Generaete a filename
        Dim LocalDate As DateTime = DateTime.Now
        Dim Filename As String = String.Format("Fullscreen_{0}-{1}-{2}_{3}{4}{5}.{6}", LocalDate.Year, LocalDate.Month, LocalDate.Day, LocalDate.Hour, LocalDate.Minute, LocalDate.Second, My.Settings.ImageFormat.ToLower)
        Dim IntendedSaveLocation As String = Path.Combine(ConfigurationClassReference.UploadsDirectory, Filename)

        'Save the file
        PlaceholderBitmap.Save(IntendedSaveLocation, ImageFormatToImageType(My.Settings.ImageFormat))

        'Return the path to the function that called
        Return IntendedSaveLocation
    End Function

    Private Declare Function SetSystemCursor Lib "user32.dll" (ByVal hCursor As IntPtr, ByVal id As Integer) As Boolean

    Public Function Selection()

        'Clear Positons
        ClickedPosition1 = NeutalClickingPoint
        ClickedPosition2 = NeutalClickingPoint

        'Store the current system cursor
        Dim OriginalCursor As Icon = Icon.FromHandle(Cursor.Current.CopyHandle)

        'Cursor constants
        Const IDC_ARROW As UInt32 = 32512
        Const IDC_CROSS As UInt32 = 32515

        'Change Active Cursor
        SetSystemCursor(IDC_CROSS, IDC_CROSS)

        Waiter1 = True
        While Waiter1
            'Just some data to stall around
        End While

        Threading.Thread.Sleep(200)

        waiter2 = True
        While waiter2
            'Just some data to stall around
        End While

        'Change Active Cursor
        SetSystemCursor(IDC_ARROW, IDC_ARROW)

        MsgBox(ClickedPosition1.ToString)
        MsgBox(ClickedPosition1.ToString)
    End Function

    Public Function Clipboard()

        'Placeholder for making a filename later
        Dim LocalDate As DateTime = DateTime.Now

        If My.Computer.Clipboard.ContainsText Then

            'Generaete a filename
            Dim Filename As String = String.Format("Clipboard_{0}-{1}-{2}_{3}{4}{5}.{6}", LocalDate.Year, LocalDate.Month, LocalDate.Day, LocalDate.Hour, LocalDate.Minute, LocalDate.Second, "txt")
            Dim IntendedSaveLocation As String = Path.Combine(ConfigurationClassReference.UploadsDirectory, Filename)

            'Save file and upload
            File.WriteAllText(IntendedSaveLocation, My.Computer.Clipboard.GetText)

            'Return to the function that called.
            Return IntendedSaveLocation

        ElseIf My.Computer.Clipboard.ContainsImage Then
            'Pull Data from the clipboard.
            Dim LocalDataObject As IDataObject = System.Windows.Forms.Clipboard.GetDataObject()

            'Check to make sure what we have is image formatted
            If LocalDataObject.GetDataPresent(System.Windows.Forms.DataFormats.Bitmap) Then

                'Get the image from the local data object
                Dim LocalImageObject As System.Drawing.Image = LocalDataObject.GetData(DataFormats.Bitmap, True)

                'Generaete a filename
                Dim Filename As String = String.Format("Clipboard_{0}-{1}-{2}_{3}{4}{5}.{6}", LocalDate.Year, LocalDate.Month, LocalDate.Day, LocalDate.Hour, LocalDate.Minute, LocalDate.Second, My.Settings.ImageFormat)
                Dim IntendedSaveLocation As String = Path.Combine(ConfigurationClassReference.UploadsDirectory, Filename)

                'Save the file
                LocalImageObject.Save(IntendedSaveLocation, ImageFormatToImageType(My.Settings.ImageFormat))

                'return to the parent function
                Return IntendedSaveLocation
            Else
                Return False
            End If
        ElseIf My.Computer.Clipboard.ContainsFileDropList Then
            If My.Computer.Clipboard.GetFileDropList.Count = 1 Then

                'Return the single file to the parent function
                Return My.Computer.Clipboard.GetFileDropList.Item(0)

            ElseIf My.Computer.Clipboard.GetFileDropList.Count > 1 Then
                'There is multiple files, and we should compress them

                'Create placeholder variables for what we're about to compress, along with a filename
                Dim FilenamePrefix As String = String.Format("Clipboard_Combined_{0}-{1}-{2}_{3}{4}{5}", LocalDate.Year, LocalDate.Month, LocalDate.Day, LocalDate.Hour, LocalDate.Minute, LocalDate.Second)
                Dim LocalArchiveDirectory As String = Path.Combine(ConfigurationClassReference.UploadsDirectory, FilenamePrefix)
                Dim LocalArchiveLocation As String = Path.Combine(ConfigurationClassReference.UploadsDirectory, String.Format("{0}.{1}", FilenamePrefix, "zip"))

                'Create the directory we will be working in
                If My.Computer.FileSystem.DirectoryExists(LocalArchiveDirectory) = False Then My.Computer.FileSystem.CreateDirectory(LocalArchiveDirectory)

                'Copy the files to the directory.
                For Each FileToCopy In My.Computer.Clipboard.GetFileDropList
                    My.Computer.FileSystem.CopyFile(FileToCopy, Path.Combine(LocalArchiveDirectory, Path.GetFileName(FileToCopy)))
                Next

                'Compress the directory into an archive
                ZipFile.CreateFromDirectory(LocalArchiveDirectory, LocalArchiveLocation)

                'Delete the directory that was used to compress files to save space.
                My.Computer.FileSystem.DeleteDirectory(LocalArchiveDirectory, FileIO.DeleteDirectoryOption.DeleteAllContents)

                'Return to the function that called the location of the archive
                Return LocalArchiveLocation
            End If
        Else
            Return False
        End If
    End Function

    Private Function ImageFormatToImageType(ByVal type As String) As System.Drawing.Imaging.ImageFormat
        Select Case type.ToLower
            Case "png"
                Return System.Drawing.Imaging.ImageFormat.Png
            Case "jpg", "jpeg"
                Return System.Drawing.Imaging.ImageFormat.Jpeg
            Case "bmp"
                Return System.Drawing.Imaging.ImageFormat.Bmp
            Case "tiff"
                Return System.Drawing.Imaging.ImageFormat.Tiff
            Case Else
                Return System.Drawing.Imaging.ImageFormat.Png
        End Select
    End Function

    Private Sub MouseDetector_MouseLeftButtonClick(ByVal sender As Object, ByVal e As MouseEventArgs) Handles MouseDetector.MouseLeftButtonClick

        If Waiter1 Then
            ClickedPosition1 = e.Location
            Waiter1 = False
        End If

        If waiter2 Then
            ClickedPosition2 = e.Location
            waiter2 = False
        End If

es1:
    End Sub

End Class
