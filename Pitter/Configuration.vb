﻿Imports System.Environment
Imports System.IO

Public Class Configuration
    'Directories - This controls the base layout of the application
    Public UploadsDirectory As String = Path.Combine(FileIO.SpecialDirectories.MyDocuments, "Pitter")

    'Update Repository
    Public SquirrelUpdateURL As String = "https://gitlab.com/pitter-llc/Windows-Desktop-Client/raw/master/Pitter/Releases/"

    'Default Notification Timeout Value - Toasts
    Public DefaultNotificationTimeout As Integer = 5000


    Public Sub New()
        MainForm.EasyLog("Configration\Constructor", "Class has been initialized")

        'Check to see if the uploads directory exists
        If Not My.Computer.FileSystem.DirectoryExists(UploadsDirectory) Then
            MainForm.EasyLog("Configration\Constructor", "Creating uploads directory: " + UploadsDirectory)
            My.Computer.FileSystem.CreateDirectory(UploadsDirectory)
        End If
    End Sub

    Public Sub ResetSettings()
        MainForm.EasyLog("Configration\Reset", "ERASING ALL SETTINGS")
        My.Settings.Reload()
        My.Settings.Reset()
        My.Settings.Save()
    End Sub

End Class
