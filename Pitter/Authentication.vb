﻿Imports Newtonsoft.Json.Linq

Public Class Authentication
    'Class References
    Private ConfigurationClass As Configuration

    Public Sub New(ByVal PassedConfigurationClass As Configuration)
        MainForm.EasyLog("Authentication\Constructor", "Class has been initialized")
        'Class Inheritence
        ConfigurationClass = PassedConfigurationClass
    End Sub

    Public Function Authenticate()
        MainForm.EasyLog("Authentication\Authenticate", "Attempting to authenticate with the server")
        Using NetworkAuthenticationClient As New Net.WebClient
            'Change User Agent
            NetworkAuthenticationClient.Headers.Add("User-Agent", "Pitter/" + My.Settings.AccountEmailAddress)

            'Create POST Fields
            Dim NetworkAuthenticationClientParameters As New Specialized.NameValueCollection

            'Tack fields
            NetworkAuthenticationClientParameters.Add("email", My.Settings.AccountEmailAddress)
            NetworkAuthenticationClientParameters.Add("password", My.Settings.AccountPassword)

            'Make and return request
            Try
                Dim RemoteResponse = NetworkAuthenticationClient.UploadValues("https://pitterapp.com/client/login", "POST", NetworkAuthenticationClientParameters)
                Dim ConvertedString = System.Text.Encoding.UTF8.GetString(RemoteResponse)

                MainForm.EasyLog("Authentication\Server Response", ConvertedString)

                Return JObject.Parse(ConvertedString).GetValue("status")
            Catch ex As Exception
                MsgBox("Service Error: The response returned from pitter servers was invalid, there is an internal issue.")
                MsgBox(ex.ToString)
                Process.GetCurrentProcess.Kill()
            End Try
        End Using
    End Function

    Public Sub SaveCredentials(ByVal PassedEmailAddress As String, ByVal PassedPassword As String)

        'Get a fresh instance of settings
        MainForm.EasyLog("Authentication\SaveCredentials", "Reloading settings instance...")
        My.Settings.Reload()

        'Write Encrypted email into data store
        My.Settings.AccountEmailAddress = PassedEmailAddress
        MainForm.EasyLog("Authentication\SaveCredentials", "Saving email...")

        'Write Encrypted password into data store
        MainForm.EasyLog("Authentication\SaveCredentials", "Saving password...")
        My.Settings.AccountPassword = PassedPassword

        'Save settings
        MainForm.EasyLog("Authentication\SaveCredentials", "Attempting to write credentials into settings")
        My.Settings.Save()
    End Sub

End Class
