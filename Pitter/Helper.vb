﻿Imports System.IO
Imports System.Runtime.InteropServices

Public Class Helper

    Private MasterForm As MainForm
    Private ConfigurationClass As Configuration

    'If any of these process names below are detected, the interval of which the detection delays itself decreases.
    Public HighPerofrmanceProcesses As String() = {
        "gameoverlayui", ' Steam Game Overlay (will work for any game on steam)
        "lolclient", ' League of Legends
        "wow", ' World of Warcraft
        "Overwatch", ' Overwatch
        "osu", ' Osu!
        "bf3", "bf4", "bf1", ' Battlefield Series
        "MinecraftLauncher" 'Minecraft (Launcher)
    }

    Public Structure KeybindStructure
        Public id As Integer
        Public label As String
    End Structure
    Public KeyList As List(Of KeybindStructure)

    Public Sub New(ByVal PassedMasterForm As MainForm, ByVal PassedConfigurationClass As Configuration)
        MainForm.EasyLog("Helper\Constructor", "Class has been initialized")
        MasterForm = PassedMasterForm
        ConfigurationClass = PassedConfigurationClass
        KeyList = New List(Of KeybindStructure)
        PopulateKeys()
    End Sub

    Public Function ConnectedToInternet(ByVal DisplayNotification As Boolean)
        MainForm.EasyLog("Helper\Connection", "Checking internet connection status")
        Dim BooleanResponse = My.Computer.Network.IsAvailable

        If DisplayNotification And BooleanResponse Then
            MainForm.EasyLog("Helper\Connection", "A connection to the internet does not appear to be present")
            MasterForm.MainNotificationIcon.BalloonTipTitle = "Internet Connection Unavailable"
            MasterForm.MainNotificationIcon.BalloonTipText = "Pitter cannot upload this file as your computer is not connected to the internet"
            MasterForm.MainNotificationIcon.ShowBalloonTip(ConfigurationClass.DefaultNotificationTimeout)
        Else
            MainForm.EasyLog("Helper\Connection", "We have internet!")
        End If

        Return BooleanResponse
    End Function

    Private Sub PopulateKeys()
        MainForm.EasyLog("Helper\Populate Keys", "Adding Characters to Structure List")
        Dim LocalStruct As New KeybindStructure

        'Numeric 0
        LocalStruct.id = 48
        LocalStruct.label = "0 (Number Row)"
        KeyList.Add(LocalStruct)

        'Numeric 1
        LocalStruct.id = 49
        LocalStruct.label = "1 (Number Row)"
        KeyList.Add(LocalStruct)

        'Numeric 2
        LocalStruct.id = 50
        LocalStruct.label = "2 (Number Row)"
        KeyList.Add(LocalStruct)

        'Numeric 3
        LocalStruct.id = 51
        LocalStruct.label = "3 (Number Row)"
        KeyList.Add(LocalStruct)

        'Numeric 4
        LocalStruct.id = 52
        LocalStruct.label = "4 (Number Row)"
        KeyList.Add(LocalStruct)

        'Numeric 5
        LocalStruct.id = 53
        LocalStruct.label = "5 (Number Row)"
        KeyList.Add(LocalStruct)

        'Numeric 6
        LocalStruct.id = 54
        LocalStruct.label = "6 (Number Row)"
        KeyList.Add(LocalStruct)

        'Numeric 7
        LocalStruct.id = 55
        LocalStruct.label = "7 (Number Row)"
        KeyList.Add(LocalStruct)

        'Numeric 8
        LocalStruct.id = 56
        LocalStruct.label = "8 (Number Row)"
        KeyList.Add(LocalStruct)

        'Numeric 9
        LocalStruct.id = 57
        LocalStruct.label = "9 (Number Row)"
        KeyList.Add(LocalStruct)

        'Numpad 0
        LocalStruct.id = 96
        LocalStruct.label = "0 (Numpad)"
        KeyList.Add(LocalStruct)

        'Numpad 1
        LocalStruct.id = 97
        LocalStruct.label = "1 (Numpad)"
        KeyList.Add(LocalStruct)

        'Numpad 2
        LocalStruct.id = 98
        LocalStruct.label = "2 (Numpad)"
        KeyList.Add(LocalStruct)

        'Numpad 3
        LocalStruct.id = 99
        LocalStruct.label = "3 (Numpad)"
        KeyList.Add(LocalStruct)

        'Numpad 4
        LocalStruct.id = 100
        LocalStruct.label = "4 (Numpad)"
        KeyList.Add(LocalStruct)

        'Numpad 5
        LocalStruct.id = 101
        LocalStruct.label = "5 (Numpad)"
        KeyList.Add(LocalStruct)

        'Numpad 6
        LocalStruct.id = 102
        LocalStruct.label = "6 (Numpad)"
        KeyList.Add(LocalStruct)

        'Numpad 7
        LocalStruct.id = 103
        LocalStruct.label = "7 (Numpad)"
        KeyList.Add(LocalStruct)

        'Numpad 8
        LocalStruct.id = 104
        LocalStruct.label = "8 (Numpad)"
        KeyList.Add(LocalStruct)

        'Numpad 9
        LocalStruct.id = 105
        LocalStruct.label = "9 (Numpad)"
        KeyList.Add(LocalStruct)

        'Numpad *
        LocalStruct.id = 106
        LocalStruct.label = "* (Numpad)"
        KeyList.Add(LocalStruct)

        'Numpad +
        LocalStruct.id = 107
        LocalStruct.label = "+ (Numpad)"
        KeyList.Add(LocalStruct)

        'Numpad -
        LocalStruct.id = 109
        LocalStruct.label = "- (Numpad)"
        KeyList.Add(LocalStruct)

        'Letter A
        LocalStruct.id = 65
        LocalStruct.label = "A"
        KeyList.Add(LocalStruct)

        'Letter B
        LocalStruct.id = 66
        LocalStruct.label = "B"
        KeyList.Add(LocalStruct)

        'Letter C
        LocalStruct.id = 67
        LocalStruct.label = "C"
        KeyList.Add(LocalStruct)

        'Letter D
        LocalStruct.id = 68
        LocalStruct.label = "D"
        KeyList.Add(LocalStruct)

        'Letter E
        LocalStruct.id = 69
        LocalStruct.label = "E"
        KeyList.Add(LocalStruct)

        'Letter F
        LocalStruct.id = 70
        LocalStruct.label = "F"
        KeyList.Add(LocalStruct)

        'Letter G
        LocalStruct.id = 71
        LocalStruct.label = "G"
        KeyList.Add(LocalStruct)

        'Letter H
        LocalStruct.id = 72
        LocalStruct.label = "H"
        KeyList.Add(LocalStruct)

        'Letter I
        LocalStruct.id = 73
        LocalStruct.label = "I"
        KeyList.Add(LocalStruct)

        'Letter J
        LocalStruct.id = 74
        LocalStruct.label = "J"
        KeyList.Add(LocalStruct)

        'Letter K
        LocalStruct.id = 75
        LocalStruct.label = "K"
        KeyList.Add(LocalStruct)

        'Letter L
        LocalStruct.id = 76
        LocalStruct.label = "L"
        KeyList.Add(LocalStruct)

        'Letter M
        LocalStruct.id = 77
        LocalStruct.label = "M"
        KeyList.Add(LocalStruct)

        'Letter N
        LocalStruct.id = 78
        LocalStruct.label = "N"
        KeyList.Add(LocalStruct)

        'Letter O
        LocalStruct.id = 79
        LocalStruct.label = "O"
        KeyList.Add(LocalStruct)

        'Letter P
        LocalStruct.id = 80
        LocalStruct.label = "P"
        KeyList.Add(LocalStruct)

        'Letter Q
        LocalStruct.id = 81
        LocalStruct.label = "Q"
        KeyList.Add(LocalStruct)

        'Letter R
        LocalStruct.id = 82
        LocalStruct.label = "R"
        KeyList.Add(LocalStruct)

        'Letter S
        LocalStruct.id = 83
        LocalStruct.label = "S"
        KeyList.Add(LocalStruct)

        'Letter T
        LocalStruct.id = 84
        LocalStruct.label = "T"
        KeyList.Add(LocalStruct)

        'Letter U
        LocalStruct.id = 85
        LocalStruct.label = "U"
        KeyList.Add(LocalStruct)

        'Letter V
        LocalStruct.id = 86
        LocalStruct.label = "V"
        KeyList.Add(LocalStruct)

        'Letter W
        LocalStruct.id = 87
        LocalStruct.label = "W"
        KeyList.Add(LocalStruct)

        'Letter X
        LocalStruct.id = 88
        LocalStruct.label = "X"
        KeyList.Add(LocalStruct)

        'Letter Y
        LocalStruct.id = 89
        LocalStruct.label = "Y"
        KeyList.Add(LocalStruct)

        'Letter Z
        LocalStruct.id = 90
        LocalStruct.label = "Z"
        KeyList.Add(LocalStruct)

    End Sub

    Public Sub PopulateCombobox(ByRef ComboBox As ComboBox)
        MainForm.EasyLog("Helper\Combobox Populator", "Adding " + KeyList.Count.ToString + " keys to " + ComboBox.Name)
        For Each key As KeybindStructure In KeyList
            ComboBox.Items.Add(key.label)
        Next
    End Sub

    Public Sub LoadComboboxSetting(ByRef ComboBox As ComboBox, ByVal DesiredValue As Integer)
        For Each key As KeybindStructure In KeyList
            If key.id = DesiredValue Then
                Dim index = KeyList.IndexOf(key)
                MainForm.EasyLog("Helper\Combobox Setting Getter", "Setting value " + index.ToString + " for " + ComboBox.Name)
                ComboBox.SelectedIndex = index
                Return
            End If
        Next
    End Sub

    Public Shared Function GetMimeType(ByVal file As String) As String
        MainForm.EasyLog("Helper\Mime Detection", "Getting Mime Type for file: " + file)
        Dim mime As String = Nothing
        Dim MaxContent As Integer = CInt(New FileInfo(file).Length)
        If MaxContent > 4096 Then
            MaxContent = 4096
        End If
        Dim fbn = Path.GetFileName(file)
        My.Computer.FileSystem.CopyFile(file, Path.Combine(Path.GetFullPath(Environment.SpecialFolder.ApplicationData), "temp-" + fbn))

        Dim fs As New FileStream(Path.Combine(Path.GetFullPath(Environment.SpecialFolder.ApplicationData), "temp-" + fbn), FileMode.Open)

        Dim buf(MaxContent) As Byte
        fs.Read(buf, 0, MaxContent)
        fs.Close()

        My.Computer.FileSystem.DeleteFile(Path.Combine(Path.GetFullPath(Environment.SpecialFolder.ApplicationData), "temp-" + fbn))

        Dim result As Integer = FindMimeFromData(IntPtr.Zero, file, buf, MaxContent, Nothing, 0, mime, 0)
        MainForm.EasyLog("Helper\Mime Detection", file + " is a " + mime)
        Return mime
    End Function

    <DllImport("urlmon.dll", CharSet:=CharSet.Auto)>
    Private Shared Function FindMimeFromData(
        ByVal pBC As IntPtr,
        <MarshalAs(UnmanagedType.LPWStr)>
        ByVal pwzUrl As String,
        <MarshalAs(UnmanagedType.LPArray, ArraySubType:=UnmanagedType.I1, SizeParamIndex:=3)> ByVal _
        pBuffer As Byte(),
        ByVal cbSize As Integer,
        <MarshalAs(UnmanagedType.LPWStr)>
        ByVal pwzMimeProposed As String,
        ByVal dwMimeFlags As Integer,
        <MarshalAs(UnmanagedType.LPWStr)>
        ByRef ppwzMimeOut As String,
        ByVal dwReserved As Integer) As Integer
    End Function
End Class
