﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
        Me.MainNotificationIcon = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ExitToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.MainStatusStrip = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatus = New System.Windows.Forms.ToolStripStatusLabel()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LogoutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ChangeCredentialsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.RestartApplicationToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CaptureToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OptionsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImageFormatToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FileFormatComboBox = New System.Windows.Forms.ToolStripComboBox()
        Me.EncryptUploadsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DynamicallyAdjustProcessPriorityToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DiagnosticsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MemoryUsageToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.CPUUsageToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProcessAffinityToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LastProcessAdjustmentToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.CheckForUpdateToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HowToUseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ApplicationStatisticWorker = New System.ComponentModel.BackgroundWorker()
        Me.KeybindsLabel = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.UploadClipboardComboBox = New System.Windows.Forms.ComboBox()
        Me.UploadScreenSelectionComboBox = New System.Windows.Forms.ComboBox()
        Me.UploadFullscreenComboBox = New System.Windows.Forms.ComboBox()
        Me.UploadCurrentWindowComboBox = New System.Windows.Forms.ComboBox()
        Me.UploadFileDialogComboBox = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.RadioButton3 = New System.Windows.Forms.RadioButton()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Splitter1 = New System.Windows.Forms.Splitter()
        Me.UpdateIntervalWorker = New System.ComponentModel.BackgroundWorker()
        Me.HideFormWorker = New System.ComponentModel.BackgroundWorker()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.MainStatusStrip.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MainNotificationIcon
        '
        Me.MainNotificationIcon.ContextMenuStrip = Me.ContextMenuStrip1
        Me.MainNotificationIcon.Text = "Pitter"
        Me.MainNotificationIcon.Visible = True
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExitToolStripMenuItem1})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(93, 26)
        '
        'ExitToolStripMenuItem1
        '
        Me.ExitToolStripMenuItem1.Name = "ExitToolStripMenuItem1"
        Me.ExitToolStripMenuItem1.Size = New System.Drawing.Size(92, 22)
        Me.ExitToolStripMenuItem1.Text = "Exit"
        '
        'MainStatusStrip
        '
        Me.MainStatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatus})
        Me.MainStatusStrip.Location = New System.Drawing.Point(0, 233)
        Me.MainStatusStrip.Name = "MainStatusStrip"
        Me.MainStatusStrip.Size = New System.Drawing.Size(628, 22)
        Me.MainStatusStrip.TabIndex = 0
        Me.MainStatusStrip.Text = "MainStatusStrip"
        '
        'ToolStripStatus
        '
        Me.ToolStripStatus.Name = "ToolStripStatus"
        Me.ToolStripStatus.Size = New System.Drawing.Size(26, 17)
        Me.ToolStripStatus.Text = "Idle"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.CaptureToolStripMenuItem, Me.OptionsToolStripMenuItem, Me.HelpToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(628, 24)
        Me.MenuStrip1.TabIndex = 1
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LogoutToolStripMenuItem, Me.ToolStripSeparator2, Me.RestartApplicationToolStripMenuItem, Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'LogoutToolStripMenuItem
        '
        Me.LogoutToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ChangeCredentialsToolStripMenuItem})
        Me.LogoutToolStripMenuItem.Name = "LogoutToolStripMenuItem"
        Me.LogoutToolStripMenuItem.Size = New System.Drawing.Size(174, 22)
        Me.LogoutToolStripMenuItem.Text = "Account"
        '
        'ChangeCredentialsToolStripMenuItem
        '
        Me.ChangeCredentialsToolStripMenuItem.Name = "ChangeCredentialsToolStripMenuItem"
        Me.ChangeCredentialsToolStripMenuItem.Size = New System.Drawing.Size(177, 22)
        Me.ChangeCredentialsToolStripMenuItem.Text = "Change Credentials"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(171, 6)
        '
        'RestartApplicationToolStripMenuItem
        '
        Me.RestartApplicationToolStripMenuItem.Name = "RestartApplicationToolStripMenuItem"
        Me.RestartApplicationToolStripMenuItem.Size = New System.Drawing.Size(174, 22)
        Me.RestartApplicationToolStripMenuItem.Text = "Restart Application"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(174, 22)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'CaptureToolStripMenuItem
        '
        Me.CaptureToolStripMenuItem.Enabled = False
        Me.CaptureToolStripMenuItem.Name = "CaptureToolStripMenuItem"
        Me.CaptureToolStripMenuItem.Size = New System.Drawing.Size(61, 20)
        Me.CaptureToolStripMenuItem.Text = "Capture"
        '
        'OptionsToolStripMenuItem
        '
        Me.OptionsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ImageFormatToolStripMenuItem, Me.EncryptUploadsToolStripMenuItem, Me.DynamicallyAdjustProcessPriorityToolStripMenuItem})
        Me.OptionsToolStripMenuItem.Name = "OptionsToolStripMenuItem"
        Me.OptionsToolStripMenuItem.Size = New System.Drawing.Size(61, 20)
        Me.OptionsToolStripMenuItem.Text = "Options"
        '
        'ImageFormatToolStripMenuItem
        '
        Me.ImageFormatToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileFormatComboBox})
        Me.ImageFormatToolStripMenuItem.Name = "ImageFormatToolStripMenuItem"
        Me.ImageFormatToolStripMenuItem.Size = New System.Drawing.Size(260, 22)
        Me.ImageFormatToolStripMenuItem.Text = "Image Format"
        '
        'FileFormatComboBox
        '
        Me.FileFormatComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.FileFormatComboBox.Items.AddRange(New Object() {"PNG", "JPG", "GIF", "TIFF"})
        Me.FileFormatComboBox.Name = "FileFormatComboBox"
        Me.FileFormatComboBox.Size = New System.Drawing.Size(121, 23)
        '
        'EncryptUploadsToolStripMenuItem
        '
        Me.EncryptUploadsToolStripMenuItem.CheckOnClick = True
        Me.EncryptUploadsToolStripMenuItem.Name = "EncryptUploadsToolStripMenuItem"
        Me.EncryptUploadsToolStripMenuItem.Size = New System.Drawing.Size(260, 22)
        Me.EncryptUploadsToolStripMenuItem.Text = "Encrypt Uploads"
        '
        'DynamicallyAdjustProcessPriorityToolStripMenuItem
        '
        Me.DynamicallyAdjustProcessPriorityToolStripMenuItem.CheckOnClick = True
        Me.DynamicallyAdjustProcessPriorityToolStripMenuItem.Name = "DynamicallyAdjustProcessPriorityToolStripMenuItem"
        Me.DynamicallyAdjustProcessPriorityToolStripMenuItem.Size = New System.Drawing.Size(260, 22)
        Me.DynamicallyAdjustProcessPriorityToolStripMenuItem.Text = "Dynamically Adjust Process Priority"
        Me.DynamicallyAdjustProcessPriorityToolStripMenuItem.ToolTipText = resources.GetString("DynamicallyAdjustProcessPriorityToolStripMenuItem.ToolTipText")
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DiagnosticsToolStripMenuItem, Me.ToolStripSeparator1, Me.CheckForUpdateToolStripMenuItem, Me.HowToUseToolStripMenuItem})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'DiagnosticsToolStripMenuItem
        '
        Me.DiagnosticsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MemoryUsageToolStripMenuItem, Me.ToolStripSeparator3, Me.CPUUsageToolStripMenuItem, Me.ProcessAffinityToolStripMenuItem, Me.LastProcessAdjustmentToolStripMenuItem})
        Me.DiagnosticsToolStripMenuItem.Name = "DiagnosticsToolStripMenuItem"
        Me.DiagnosticsToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.DiagnosticsToolStripMenuItem.Text = "Diagnostics"
        '
        'MemoryUsageToolStripMenuItem
        '
        Me.MemoryUsageToolStripMenuItem.Name = "MemoryUsageToolStripMenuItem"
        Me.MemoryUsageToolStripMenuItem.Size = New System.Drawing.Size(213, 22)
        Me.MemoryUsageToolStripMenuItem.Text = "Memory Usage"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(210, 6)
        '
        'CPUUsageToolStripMenuItem
        '
        Me.CPUUsageToolStripMenuItem.Name = "CPUUsageToolStripMenuItem"
        Me.CPUUsageToolStripMenuItem.Size = New System.Drawing.Size(213, 22)
        Me.CPUUsageToolStripMenuItem.Text = "CPU Usage:"
        Me.CPUUsageToolStripMenuItem.ToolTipText = "This is a live representation of how much of your processor is being used." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "If th" &
    "e value exceeds 90% the application will de-prioritize itself to provide more pe" &
    "rformance to other applications."
        '
        'ProcessAffinityToolStripMenuItem
        '
        Me.ProcessAffinityToolStripMenuItem.Name = "ProcessAffinityToolStripMenuItem"
        Me.ProcessAffinityToolStripMenuItem.Size = New System.Drawing.Size(213, 22)
        Me.ProcessAffinityToolStripMenuItem.Text = "Process Affinity: Unknown"
        Me.ProcessAffinityToolStripMenuItem.ToolTipText = resources.GetString("ProcessAffinityToolStripMenuItem.ToolTipText")
        '
        'LastProcessAdjustmentToolStripMenuItem
        '
        Me.LastProcessAdjustmentToolStripMenuItem.Name = "LastProcessAdjustmentToolStripMenuItem"
        Me.LastProcessAdjustmentToolStripMenuItem.Size = New System.Drawing.Size(213, 22)
        Me.LastProcessAdjustmentToolStripMenuItem.Text = "Last Process Adjustment"
        Me.LastProcessAdjustmentToolStripMenuItem.ToolTipText = "This is a notification of when the application last made an adjustment"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(165, 6)
        '
        'CheckForUpdateToolStripMenuItem
        '
        Me.CheckForUpdateToolStripMenuItem.Name = "CheckForUpdateToolStripMenuItem"
        Me.CheckForUpdateToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.CheckForUpdateToolStripMenuItem.Text = "Check For Update"
        '
        'HowToUseToolStripMenuItem
        '
        Me.HowToUseToolStripMenuItem.Name = "HowToUseToolStripMenuItem"
        Me.HowToUseToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.HowToUseToolStripMenuItem.Text = "How To Use"
        '
        'ApplicationStatisticWorker
        '
        '
        'KeybindsLabel
        '
        Me.KeybindsLabel.AutoSize = True
        Me.KeybindsLabel.Location = New System.Drawing.Point(12, 11)
        Me.KeybindsLabel.Name = "KeybindsLabel"
        Me.KeybindsLabel.Size = New System.Drawing.Size(50, 13)
        Me.KeybindsLabel.TabIndex = 3
        Me.KeybindsLabel.Text = "Keybinds"
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.UploadClipboardComboBox)
        Me.Panel1.Controls.Add(Me.UploadScreenSelectionComboBox)
        Me.Panel1.Controls.Add(Me.UploadFullscreenComboBox)
        Me.Panel1.Controls.Add(Me.UploadCurrentWindowComboBox)
        Me.Panel1.Controls.Add(Me.UploadFileDialogComboBox)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.CheckBox2)
        Me.Panel1.Controls.Add(Me.CheckBox1)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.RadioButton3)
        Me.Panel1.Controls.Add(Me.RadioButton2)
        Me.Panel1.Controls.Add(Me.RadioButton1)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.KeybindsLabel)
        Me.Panel1.Controls.Add(Me.Splitter1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 24)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(628, 209)
        Me.Panel1.TabIndex = 4
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(12, 160)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(88, 13)
        Me.Label9.TabIndex = 25
        Me.Label9.Text = "Upload Clipboard"
        '
        'UploadClipboardComboBox
        '
        Me.UploadClipboardComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.UploadClipboardComboBox.FormattingEnabled = True
        Me.UploadClipboardComboBox.Location = New System.Drawing.Point(171, 157)
        Me.UploadClipboardComboBox.Name = "UploadClipboardComboBox"
        Me.UploadClipboardComboBox.Size = New System.Drawing.Size(199, 21)
        Me.UploadClipboardComboBox.TabIndex = 24
        '
        'UploadScreenSelectionComboBox
        '
        Me.UploadScreenSelectionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.UploadScreenSelectionComboBox.Enabled = False
        Me.UploadScreenSelectionComboBox.FormattingEnabled = True
        Me.UploadScreenSelectionComboBox.Location = New System.Drawing.Point(171, 130)
        Me.UploadScreenSelectionComboBox.Name = "UploadScreenSelectionComboBox"
        Me.UploadScreenSelectionComboBox.Size = New System.Drawing.Size(199, 21)
        Me.UploadScreenSelectionComboBox.TabIndex = 23
        '
        'UploadFullscreenComboBox
        '
        Me.UploadFullscreenComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.UploadFullscreenComboBox.FormattingEnabled = True
        Me.UploadFullscreenComboBox.Location = New System.Drawing.Point(171, 103)
        Me.UploadFullscreenComboBox.Name = "UploadFullscreenComboBox"
        Me.UploadFullscreenComboBox.Size = New System.Drawing.Size(199, 21)
        Me.UploadFullscreenComboBox.TabIndex = 22
        '
        'UploadCurrentWindowComboBox
        '
        Me.UploadCurrentWindowComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.UploadCurrentWindowComboBox.FormattingEnabled = True
        Me.UploadCurrentWindowComboBox.Location = New System.Drawing.Point(171, 76)
        Me.UploadCurrentWindowComboBox.Name = "UploadCurrentWindowComboBox"
        Me.UploadCurrentWindowComboBox.Size = New System.Drawing.Size(199, 21)
        Me.UploadCurrentWindowComboBox.TabIndex = 21
        '
        'UploadFileDialogComboBox
        '
        Me.UploadFileDialogComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.UploadFileDialogComboBox.FormattingEnabled = True
        Me.UploadFileDialogComboBox.Location = New System.Drawing.Point(171, 48)
        Me.UploadFileDialogComboBox.Name = "UploadFileDialogComboBox"
        Me.UploadFileDialogComboBox.Size = New System.Drawing.Size(199, 21)
        Me.UploadFileDialogComboBox.TabIndex = 20
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(12, 133)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(128, 13)
        Me.Label8.TabIndex = 19
        Me.Label8.Text = "Capture Screen Selection"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(12, 106)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(95, 13)
        Me.Label7.TabIndex = 18
        Me.Label7.Text = "Capture Fullscreen"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(12, 79)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(123, 13)
        Me.Label6.TabIndex = 17
        Me.Label6.Text = "Capture Current Window"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(12, 51)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(93, 13)
        Me.Label5.TabIndex = 16
        Me.Label5.Text = "Upload File Dialog"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(603, 100)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(13, 13)
        Me.Label4.TabIndex = 15
        Me.Label4.Text = "?"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(603, 11)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(13, 13)
        Me.Label3.TabIndex = 14
        Me.Label3.Text = "?"
        '
        'CheckBox2
        '
        Me.CheckBox2.AutoSize = True
        Me.CheckBox2.Location = New System.Drawing.Point(396, 50)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(108, 17)
        Me.CheckBox2.TabIndex = 13
        Me.CheckBox2.Text = "Shift key required"
        Me.CheckBox2.UseVisualStyleBackColor = True
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Location = New System.Drawing.Point(396, 27)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(120, 17)
        Me.CheckBox1.TabIndex = 12
        Me.CheckBox1.Text = "Control key required"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(393, 11)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(120, 13)
        Me.Label2.TabIndex = 11
        Me.Label2.Text = "Actuation Requirements"
        '
        'RadioButton3
        '
        Me.RadioButton3.AutoSize = True
        Me.RadioButton3.Location = New System.Drawing.Point(399, 162)
        Me.RadioButton3.Name = "RadioButton3"
        Me.RadioButton3.Size = New System.Drawing.Size(109, 17)
        Me.RadioButton3.TabIndex = 10
        Me.RadioButton3.TabStop = True
        Me.RadioButton3.Text = "Capture Selection"
        Me.RadioButton3.UseVisualStyleBackColor = True
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Location = New System.Drawing.Point(399, 139)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(113, 17)
        Me.RadioButton2.TabIndex = 9
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.Text = "Capture Fullscreen"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Location = New System.Drawing.Point(399, 116)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(77, 17)
        Me.RadioButton1.TabIndex = 8
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Do nothing"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(393, 100)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(134, 13)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Fullscreen Actuation Event"
        '
        'Splitter1
        '
        Me.Splitter1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Splitter1.Location = New System.Drawing.Point(0, 0)
        Me.Splitter1.Name = "Splitter1"
        Me.Splitter1.Size = New System.Drawing.Size(387, 207)
        Me.Splitter1.TabIndex = 4
        Me.Splitter1.TabStop = False
        '
        'UpdateIntervalWorker
        '
        '
        'HideFormWorker
        '
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(628, 255)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.MainStatusStrip)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "MainForm"
        Me.Text = "Pitter"
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.MainStatusStrip.ResumeLayout(False)
        Me.MainStatusStrip.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MainNotificationIcon As NotifyIcon
    Friend WithEvents MainStatusStrip As StatusStrip
    Friend WithEvents ToolStripStatus As ToolStripStatusLabel
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents FileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents LogoutToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CaptureToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents OptionsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents EncryptUploadsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents HelpToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CheckForUpdateToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents HowToUseToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DiagnosticsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MemoryUsageToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CPUUsageToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents ApplicationStatisticWorker As System.ComponentModel.BackgroundWorker
    Friend WithEvents ImageFormatToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents FileFormatComboBox As ToolStripComboBox
    Friend WithEvents ToolStripSeparator2 As ToolStripSeparator
    Friend WithEvents RestartApplicationToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As ToolStripSeparator
    Friend WithEvents ProcessAffinityToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents LastProcessAdjustmentToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DynamicallyAdjustProcessPriorityToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents ExitToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents KeybindsLabel As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents CheckBox2 As CheckBox
    Friend WithEvents CheckBox1 As CheckBox
    Friend WithEvents Label2 As Label
    Friend WithEvents RadioButton3 As RadioButton
    Friend WithEvents RadioButton2 As RadioButton
    Friend WithEvents RadioButton1 As RadioButton
    Friend WithEvents Label1 As Label
    Friend WithEvents Splitter1 As Splitter
    Friend WithEvents Label9 As Label
    Friend WithEvents UploadClipboardComboBox As ComboBox
    Friend WithEvents UploadScreenSelectionComboBox As ComboBox
    Friend WithEvents UploadFullscreenComboBox As ComboBox
    Friend WithEvents UploadCurrentWindowComboBox As ComboBox
    Friend WithEvents UploadFileDialogComboBox As ComboBox
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents ChangeCredentialsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents UpdateIntervalWorker As System.ComponentModel.BackgroundWorker
    Friend WithEvents HideFormWorker As System.ComponentModel.BackgroundWorker
End Class
