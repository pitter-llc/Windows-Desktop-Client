﻿Imports System.IO
Imports Newtonsoft.Json.Linq

Public Class Upload
    Private MasterForm As MainForm
    Private HelperClass As Helper
    Private ConfigClass As Configuration

    Public NewInstance As Boolean = True

    Public Sub New(ByVal PassedMasterForm As MainForm, ByVal PassedHelperClass As Helper, ByVal PassedConfigClass As Configuration)
        'Store the master form
        MasterForm = PassedMasterForm
        HelperClass = PassedHelperClass
        ConfigClass = PassedConfigClass

    End Sub

    <STAThread()>
    Public Function Upload(ByVal PassedFileToUpload As String)
        'Debug message
        MainForm.EasyLog("Upload\Upload", "Starting upload process for file: " + PassedFileToUpload)

        'Collect information about the file
        Dim FileBaseName = Path.GetFileName(PassedFileToUpload)
        Dim FileExtension = Path.GetExtension(PassedFileToUpload)

        'Determine Internet Availability
        If Not HelperClass.ConnectedToInternet(False) Then Return False

        'Create a new Multipart Form
        Dim LocalMultiPartForm As MultipartForm = New MultipartForm("https://pitterapp.com/upload")

        'Add Credentials
        TackCredentials(LocalMultiPartForm)

        'Mime override
        LocalMultiPartForm.setField("mime", Helper.GetMimeType(PassedFileToUpload))

        'Determine if we should have the file encrypted
        If My.Settings.UploadEncryption Then LocalMultiPartForm.setField("encrypt", "true")

        'Transmit the file
        LocalMultiPartForm.sendFile(PassedFileToUpload)

        NewInstance = False

        'Return the response
        ProcessResponse(LocalMultiPartForm.ResponseText.ToString)

    End Function
    <STAThread()>
    Public Sub ProcessResponse(ByVal PassedJSONString As String)
        'Debug message
        MainForm.EasyLog("Upload\Response Processor", "A response has been recieved from the server (length: " + PassedJSONString.Length.ToString + " bytes)")

        'Create an empty JObject
        Dim ParsedJSONOBject As JObject

        'Parse the passed JSON String
        Try
            ParsedJSONOBject = JObject.Parse(PassedJSONString)
        Catch ex As Exception
            'Parse Failure

            MainForm.EasyLog("Upload\Response Processor Exception", "Failed to parse the JSON data.")

            'Change the text
            MasterForm.MainNotificationIcon.BalloonTipTitle = "Invalid JSON Response"
            MasterForm.MainNotificationIcon.BalloonTipText = "The JSON Response that the server has provided is invalid, the state of the upload is unknown"
            My.Computer.Clipboard.SetText(PassedJSONString)
            GoTo END_SECTION
        End Try

        Dim StatusMessage As JObject = JObject.Parse(PassedJSONString).GetValue("type")

        Select Case StatusMessage.GetValue("bootstrap")
            Case "success", "info"
                MasterForm.MainNotificationIcon.BalloonTipIcon = ToolTipIcon.Info
                'Check to see if it was a file upload
                If ParsedJSONOBject.GetValue("title") = "Upload Successful" Then
                    'Debug message
                    MainForm.EasyLog("Upload\Response Processor", "Server returned a success")

                    'Add the URL to the clipboard
                    My.Computer.Clipboard.SetText(ParsedJSONOBject.GetValue("url"))
                End If

                'Change the tooltip icon
                MasterForm.MainNotificationIcon.BalloonTipIcon = ToolTipIcon.Info
            Case "warning"
                'Debug message
                MainForm.EasyLog("Upload\Response Processor", "Server returned a warning")

                'Change the tooltip icon
                MasterForm.MainNotificationIcon.BalloonTipIcon = ToolTipIcon.Warning
            Case "danger"
                'Debug message
                MainForm.EasyLog("Upload\Response Processor", "Server returned a critical error")

                'Change the tooltip icon
                MasterForm.MainNotificationIcon.BalloonTipIcon = ToolTipIcon.Error

            Case Else

        End Select

        'Apply the title and body to the notification icon
        MasterForm.MainNotificationIcon.BalloonTipTitle = ParsedJSONOBject.GetValue("title")
        MasterForm.MainNotificationIcon.BalloonTipText = ParsedJSONOBject.GetValue("body")

END_SECTION: 'This is the end section - we will always have a message to display.

        'Debug message
        MainForm.EasyLog("Upload\Response Processor Notification", "Sending a notification icon to the user")

        'Display it
        MasterForm.MainNotificationIcon.ShowBalloonTip(ConfigClass.DefaultNotificationTimeout)
    End Sub

    Private Sub TackCredentials(ByRef PassedMultipartForm As MultipartForm)
        'Debug message
        MainForm.EasyLog("Upload\Credentials", "Adding credentials to multi-part form")
        PassedMultipartForm.setField("email", My.Settings.AccountEmailAddress)
        PassedMultipartForm.setField("password", My.Settings.AccountPassword)
    End Sub
End Class
